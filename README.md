## Quickstart

If you know how RSS works and you want to quickly subscribe to CoTech feeds [here's the link to the OPML file](https://git.coop/cotech/rss-feeds/raw/master/feeds.opml).

## Introduction

This repository contains lists of RSS feeds published by member organisations of [CoTech](https://www.coops.tech/). It allows you to easily follow news from around the CoTech network in an RSS Reader

![Screenshot of subscriptions in Inoreader](https://git.coop/cotech/rss-feeds/raw/master/img/feed_reader.png)

## Subscribing to the feed

If you'd like to subscribe to CoTech news in your RSS reader you can grab [the raw OPML file](https://git.coop/cotech/rss-feeds/raw/master/feeds.opml) ([GitLab version](https://git.coop/cotech/rss-feeds/blob/master/feeds.opml)). In your RSS reader of choice (I like [inoreader](https://www.inoreader.com)) you might have two options:

- *Import*: grab the OPML file, save it to disk, and use your reader's "import" function to subscribe to all the feeds.
- *Subscribe*: find your feed reader's "subscribe" function and point it at `https://git.coop/cotech/rss-feeds/raw/master/feeds.opml`. The advantage of this method is that when we add or update the feeds you'll automatically see those changes in your reader.

## Updating the OPML

If you add new feeds to the lists you'll need to regenerate the OPML file. You can do that by running

    ruby generate_opml.rb

and commiting the changes to `feeds.opml`.

## Issues

I found the following blogs that, as far as I can tell, don't have RSS feeds.

- https://agile.coop/blog
- https://animorph.coop/#journal
- https://autonomic.zone/blog/
- https://outlandish.com/blog/
