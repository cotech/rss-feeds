require 'uri'

def format_blog(url)
  url = url.chomp
  host = URI.parse(url).host
  scheme = URI.parse(url).scheme

  "<outline text=\"#{host}\" title=\"#{host}\" type=\"rss\" xmlUrl=\"#{url}\" htmlUrl=\"#{scheme}://#{host}\"/>"
end

def format_twitter(url)
  url = url.chomp
  username = URI.parse(url).path.gsub('/','')
  twitrss = "https://www.twitrss.me/twitter_user_to_rss/?user=#{username}"

  "<outline text=\"@#{username}\" title=\"@#{username}\" type=\"rss\" xmlUrl=\"#{twitrss}\" htmlUrl=\"#{url}\"/>"
end

File.open('feeds.opml', 'w') do |opml|
  opml << '<?xml version="1.0" encoding="UTF-8"?>'
  opml << '<opml version="1.0">'
  opml << '<head><title>CoTech Feeds</title></head>'
  opml << '<body>'

  File.readlines('blog_feeds.txt').each do |blog|
    opml << format_blog(blog) + "\n"
  end

  File.readlines('twitter_feeds.txt').each do |twitter|
    opml << format_twitter(twitter) + "\n"
  end

  opml << '</body>'
  opml << '</opml>'
end
